# Container recepies

This repo contains an Ubuntu TF repo, which runs with openmpi on taurus.

## Build

```
sudo singularity build ubuntu_io.img ubuntu-openmpi.def
```

## run on taurus

```
module load singularity
mdoule load openmpi/3.0.0-gnu7.1
singularity exec ubuntu_io.img [prgramm]
```
